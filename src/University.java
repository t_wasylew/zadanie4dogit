import java.util.HashMap;
import java.util.Map;

public class University {

    private Map<Long, Student> studentMap = new HashMap<>();


    public void addStudent(long indexNumber, String name, String surname) {

        Student student = new Student(indexNumber, name, surname);
        studentMap.put(indexNumber,student);
    }

    public void addStudent(Student student){
        studentMap.put(student.getIndexNumber(), student);
    }

    public boolean containsStudent(long indexNumber) {
        return studentMap.containsKey(indexNumber);
    }

    public Student getStudent(long indexNumber) {
        return studentMap.get(indexNumber);
    }

    public int studentsCount() {
        return studentMap.size();
    }

    public void printAllStudnets() {
        for (Student student : studentMap.values()) {
            System.out.println(student);
        }
    }

}
